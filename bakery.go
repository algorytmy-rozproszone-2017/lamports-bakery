package main

import ("fmt"
	"time"
)

var entering []bool
var number []int

func max() int{
	max := -1
	for _, element := range number {
		if element > max {
			max = element
		}
	}
	return max
}

func lock(id int) {
	entering[id] = true
	number[id] = 1 + max()
	entering[id] = false
	for i:= 0; i < len(number); i++{
		for entering[i] {
			//fmt.Printf("%d waiting1\n", id)
			time.Sleep(50 * time.Millisecond)
		}
		for number[i] != 0 && (number[i] < number[id] || (number[i] == number[id] && i < id)) {
			//fmt.Printf("%d waiting2\n", id)
			time.Sleep(50 * time.Millisecond)
		}
	}
}

func unlock(id int) {
	number[id] = 0
}

func thread(id int) {
	for {
		fmt.Printf("%d tries to enter critical section\n", id)
		lock(id)

		fmt.Printf("%d in critical section\n", id)
		time.Sleep(50 * time.Millisecond)
		unlock(id)

		fmt.Printf("%d leaves critical section\n", id)
	}
}


func main() {
	fmt.Println("START!")

	n := 3
	entering = make([]bool, n)
	number = make([]int, n)
	for i:= 0; i < n; i++ {
		go thread(i)
	}
	for {
	}
}